import replace from "gulp-replace"; // search and replace
import plumber from "gulp-plumber"; // mistakes
import notify from "gulp-notify"; // hint messages
import browsersync from "browser-sync"; // local server
import newer from "gulp-newer"; // search for updates
import ifPlugin from "gulp-if"; // условное ветвление




// export obj
export const plugins = {
    replace: replace,
    plumber: plumber,
    notify: notify,
    browsersync: browsersync,
    newer: newer,
    if: ifPlugin
}