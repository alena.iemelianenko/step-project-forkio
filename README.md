Step Project Forkio

Учасники: Емельяненко Альона, яка вконала такі завдання для студента №1:
1. шапку сайту з верхнім меню (включаючи випадаюче меню при малій роздільній здатності екрана)
2. секцію People Are Talking About Fork.

Проект виконан за допомогою SCSS, BEM

Проект зібран Gulp. 
Основні робочі команди: 
- dev 
- build; 

При збірці використані такі пакети:
- "browser-sync",
- "del",
- "gulp",
- "gulp-autoprefixer",
- "gulp-clean-css",
- "gulp-file-include",
- "gulp-fonter",
- "gulp-group-css-media-queries",
- "gulp-if",
- "gulp-imagemin",
- "gulp-newer",
- "gulp-notify",
- "gulp-plumber",
- "gulp-rename",
- "gulp-replace",
- "gulp-sass":,
- "gulp-version-number",
- "gulp-webp",
- "gulp-webpcss",
- "sass",
- "webp-converter",
- "webpack",
- "webpack-stream";