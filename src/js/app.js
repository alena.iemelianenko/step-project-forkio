import * as flsFunctions from "./modules/functions.js";

flsFunctions.isWebp();

let burgerBtn = document.querySelector(".burger-menu__btn");
let headerMenu = document.querySelector(".header__menu");

burgerBtn.addEventListener('click', (e) =>  {
    if (e.target.closest('.burger-menu__btn')) {
        burgerBtn.classList.toggle('burger-btn--active');
        headerMenu.classList.toggle('header__menu--active');
    }
});

document.addEventListener('click', (e) => {
    if (!e.target.closest('.burger-menu__btn')) {
        headerMenu.classList.remove('header__menu--active');
    }    
});
